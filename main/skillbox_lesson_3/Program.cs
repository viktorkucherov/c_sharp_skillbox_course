﻿using System;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.Drawing;

namespace skillbox_lesson_3
{
    class Program
    {
        
        static void Main(string[] args)
        {
            #region WELCOME_PAGE
            
            Console.CursorVisible = false;
            string welcome_text = "SKILLBOX NUMERIC GAME by V.Kucherov";
            Console.Title = welcome_text;

            string pressEnter = "[PRESS ENTER]";
            int width = Console.WindowWidth;
            int height = Console.WindowHeight;
            int text_lenght = welcome_text.Length;
            int X_axis_center = (width / 2) - (text_lenght / 2);
            int Y_axis_center = (height / 2) - 1;
  
            ConsoleKeyInfo checkEnter;
            do
            {
                while (Console.KeyAvailable == false)
                {
                    Console.SetCursorPosition(X_axis_center,Y_axis_center-2);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine(welcome_text);
                    
                    Console.SetCursorPosition(X_axis_center+10,Y_axis_center);
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(pressEnter);
                    
                    System.Threading.Thread.Sleep(1000);
                    
                    Console.SetCursorPosition(X_axis_center+10,Y_axis_center);
                    Console.ForegroundColor = ConsoleColor.Black;
                    Console.WriteLine(pressEnter);
                    
                    System.Threading.Thread.Sleep(500);
                }
                checkEnter = Console.ReadKey(true);
            } while (checkEnter.Key != ConsoleKey.Enter);
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.White;
            #endregion

            #region MENU_NAVIGATION

            ConsoleKeyInfo checkKey;
            
            bool onePlayer = true;


            string onePlayerVsAI_menu = "1 PLAYER";
            string twoPlayers_menu = "2 PLAYERS";
         
            do
            {
                while (Console.KeyAvailable == false)
                {
                    Console.SetCursorPosition(X_axis_center + 11, Y_axis_center);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.WriteLine(onePlayerVsAI_menu);
                    Console.SetCursorPosition(X_axis_center + 10, Y_axis_center + 2);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.WriteLine(twoPlayers_menu);
                    
                    onePlayer = true;

                }  checkKey = Console.ReadKey(true);

                if (checkKey.Key == ConsoleKey.DownArrow)
                {
                    Console.SetCursorPosition(X_axis_center+11,Y_axis_center);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.WriteLine(onePlayerVsAI_menu);
                    Console.SetCursorPosition(X_axis_center+10,Y_axis_center+2);
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.WriteLine(twoPlayers_menu);

                    onePlayer = false;
                    
                    checkKey = Console.ReadKey(true);
                }
                
                else if (checkKey.Key == ConsoleKey.UpArrow)
                {
                        Console.SetCursorPosition(X_axis_center+11,Y_axis_center);
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.WriteLine(onePlayerVsAI_menu);
                        Console.SetCursorPosition(X_axis_center+10,Y_axis_center+2);
                        Console.ForegroundColor = ConsoleColor.Blue;
                        Console.BackgroundColor = ConsoleColor.Black;
                        Console.WriteLine(twoPlayers_menu);
                        
                        onePlayer = true;
              
                        checkKey = Console.ReadKey(true);
                }
                

            } while (checkKey.Key != ConsoleKey.Enter);
            Console.ResetColor();
            Console.Clear();
            Console.WriteLine($"One player status is {onePlayer}");
            #endregion
            
            

            //var blue = ConsoleColor.Blue;
            //typeof(blue);
      
            //var backgroundColor = Console.BackgroundColor;
            //Console.BackgroundColor = blue;
            //Console.ForegroundColor = ConsoleColor.Magenta;
            //Console.WriteLine("Hello World!");
            //Console.ResetColor();
            //var consoleKey = ConsoleKey.UpArrow;
            // var upArrow = ConsoleKey.Execute;
            //Console.WriteLine("Hello World!");
            //if (ConsoleKey.Execute == ConsoleKey.UpArrow)
            {
                
            }
            Console.ReadKey();
        }
    }


}