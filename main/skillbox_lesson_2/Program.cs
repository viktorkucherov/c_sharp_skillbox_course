﻿using System;


namespace skillbox_lesson_2
{
    class Program
    {
        static void Main(string[] args)
        {
//Домашнее задание:
// TODO Создать несколько переменных разных типов, в которых могут храниться данные, разных типов.
            
            bool true_var = true;
            bool false_var = false;
            
            byte unsigned_bit = 255;
            sbyte signed_bit = -128;

            short signed_short = -32768;
            ushort unsigned_short = 65535;

            int signed_int = -2_147_483_648;
            uint unsigned_int = 4_294_967_295;
            
            long signed_long = -9_223_372_036_854_775_808;
            ulong unsigned_long = 18_446_744_073_709_551_615;

            float float_var = 1234567890.9234567890f;
            double double_var = 1234567890.1234567890d;
            decimal decimal_var = 0.123456789m;

            char char_var = 'a';
            string string_var = "any_string";
            
            object any_var = "any_var";
            var any_var2 = "any_var2";
            
// TODO Реализовать в системе автоматический подсчёт среднего балла по трем предметам.

            byte math_mark = 5;
            byte english_mark = 5;
            byte literature_mark = 4;

            float result = ((float)math_mark + (float)english_mark + (float)literature_mark) / 3;

// TODO Реализовать возможность печати информации на консоли при помощи обычного вывода, форматированного вывода,
// TODO используя интерполяцию строк.
            
            Console.WriteLine("Простой вывод.");
            
            Console.WriteLine("Форматированный вывод с выводом валюты:\n{0:c3}", decimal_var);
            
            Console.WriteLine("Форматированный вывод в целочисенном формате с указанием мин.количества цифр:\n" +
                              "{0:d10}", unsigned_bit);
            Console.WriteLine("Форматированный вывод c экспоненциальным представлением числа:\n" +
                              "{0:e2}", unsigned_bit);
            Console.WriteLine("Форматированный вывод дробных чисел (цифра - количество знаков после запятой):\n" +
                              "{0:f2}", float_var);
            Console.WriteLine("Короткое представление числа в экспоненциальной форме):\n" +
                              "{0:g2}", float_var);
            Console.WriteLine("Форматированное представление дробного числа с " +
                              "указанием количества знаков после запятой):\n" +
                              "{0:n2}", float_var);
            Console.WriteLine("Форматированное представление дробного числа в процентной форме с " +
                              "указанием количества знаков после запятой):\n" +
                              "{0:p3}", 0.13789);
            Console.WriteLine("Форматированное представление дробного числа с " +
                              "указанием количества знаков после запятой и знаком процента):\n" +
                              "{0:x}", unsigned_bit);
            long phone_number = 89880125898;
            Console.WriteLine("my phone number is {0:#(###)###-##-##}", phone_number);
            
            // Использование интерполяции строк
            var name = "Viktor";
            var surname = "Kucherov";
            Console.WriteLine($"My name is {name} and my surname is {surname}"); 
            // TODO Весь код должен быть откомментирован с использованием обычных и хml комментариев.
            // TODO Реализовать возможность вывода данных в центре консоли.
            string text = "SKILLBOX";
            int width = Console.WindowWidth;
            int height = Console.WindowHeight;
            int text_lenght = text.Length;
            int X_axis_center = (width / 2) - (text_lenght / 2);
            int Y_axis_center = (height / 2) - 1;
            Console.SetCursorPosition(X_axis_center,Y_axis_center);
            Console.WriteLine(text);
            
            Console.ReadKey();


        }
    }
}